<div align="center">

# @chargedcloud/promise-sleep

The promise-sleep package is a simple function who returns a promise that resolves after a certain amount of time.

[Installation](#installation) • [Usage](#usage)

</div>

## Installation

You can install the package using npm:

```sh
npm install @chargedcloud/promise-sleep
```

## Usage

First, you need to import the package:

```js
// CommonJS
const sleep = require("@chargedcloud/promise-sleep");

// ES6
import sleep from "@chargedcloud/promise-sleep";
```

Then, you can use the function passing the amount of time in milliseconds:

```js
// Sleep for 1 second
await sleep(1000);

// Sleep for 1 second and then log "Hello, world!"
await sleep(1000).then(() => console.log("Hello, world!"));
```
